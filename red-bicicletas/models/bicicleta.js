var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}
//Prototipado de javascript <-- Repasar
Bicicleta.prototype.toString = function (){
    return 'id: '+ this.id + '| color: '+ this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}
Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error('No existe una bicicleta con el ID $(aBiciId)');
}

Bicicleta.removeById = function(aBiciId){
     //Bicicleta.findById(aBiciId);
    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}



var a = new Bicicleta(1, 'Verde', 'urbana', ['41.51387', '-5.73123']);
var b = new Bicicleta(2, 'Azul', 'urbana', ['41.51223', '-5.73025']);
var c = new Bicicleta(3, 'Amarila', 'Carretera', ['41.51403', '-5.73362']);
Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);

module.exports = Bicicleta;